//
//  CoinDetailModelTests.swift
//  CryptoWalletTests
//
//  Created by Tu Doan on 25/02/2022.
//

import XCTest
@testable import CryptoWallet

class CoinDetailModelTests: XCTestCase {
    var coinDetail: CoinDetailModel?
    var coinBase = "ABC"
    let buy = 100, sell = 90
    
    override func setUpWithError() throws {
        coinDetail = CoinDetailModel(base: coinBase, counter: "", buyPrice: buy.description, icon: "", sellPrice: sell.description, name: "", iconImage: nil)
    }

    override func tearDownWithError() throws {
        coinDetail = nil
        UserDefaults.standard.removeObject(forKey: coinBase)
    }

    func testDefaultIsFavorite() {
        XCTAssertEqual(coinDetail?.isFavorite, false)
    }
    
    func testIsFavorite() throws {
        coinDetail?.isFavorite = true
        let expected = UserDefaults.standard.object(forKey: coinBase) as? Bool
        XCTAssertEqual(coinDetail?.isFavorite, expected)
    }

    func testPercentChange() {
        let expected = 10.0
        XCTAssertEqual(coinDetail?.percentChange, expected)
    }
    
    func testEmptyPercentChange() {
        coinDetail?.buyPrice = "abc"
        coinDetail?.sellPrice = "abcd"
        let expected = 0.0
        XCTAssertEqual(coinDetail?.percentChange, expected)
    }
}
