//
//  NetworkTests.swift
//  CryptoWalletTests
//
//  Created by Tu Doan on 25/02/2022.
//

import XCTest
@testable import CryptoWallet

class URLSessionMock: URLSession {
    typealias CompletionHandler = (Data?, URLResponse?, Error?) -> Void

    var data: Data?
    var error: Error?
    
    override func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void)  -> URLSessionDataTask {
        let data = self.data
        let error = self.error
        
        
        return URLSessionDataTaskMock {
            completionHandler(data, nil, error)
        }
    }
    
}

class URLSessionDataTaskMock: URLSessionDataTask {
    private let closure: () -> Void

    init(closure: @escaping () -> Void) {
        self.closure = closure
    }
    
    override func resume() {
        closure()
        print("custom resume")
    }
}

class NetworkTests: XCTestCase {
    var manager: Network?
    
    override func setUpWithError() throws {
        let mockData = FakeData.shared.getJSON_Data(fileName: "currencyList")
        let session = URLSessionMock()
        session.data = mockData
        manager = Network(session: session)
    }
    
    override func tearDownWithError() throws {
        manager = nil
    }
    
    func testSuccessfulResponse() throws {
        var result: [CoinDetailModel] = []
        let exp = expectation(description: "done")
        manager?.query { data, response, dataError, error in
            DispatchQueue.main.async {
                result = data

                XCTAssertEqual(result.count, 63)
                exp.fulfill()
            }
           
        }
        
        waitForExpectations(timeout: 10.0) { error in
            XCTAssertNil(error)
        }
    }


}
