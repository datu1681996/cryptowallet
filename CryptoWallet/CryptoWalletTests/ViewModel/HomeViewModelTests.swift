//
//  HomeViewModelTests.swift
//  CryptoWalletTests
//
//  Created by Tu Doan on 25/02/2022.
//

import XCTest
@testable import CryptoWallet


extension HomeViewModelTests: HomeViewModelDelegate {
    func onComplete(isError: Bool) {
        DispatchQueue.main.async {
            self.exp?.fulfill()
        }
    }
    
}

class HomeViewModelTests: XCTestCase {
    var viewModel: HomeViewModel?
    var exp: XCTestExpectation?
    
    override func setUpWithError() throws {
        let mockData = FakeData.shared.getJSON_Data(fileName: "currencyList")
        let session = URLSessionMock()
        session.data = mockData
        let manager = Network(session: session)
        let domainModel = CoinDomainModel(coinAPI: manager)
        viewModel = HomeViewModel(coinDomainModel: domainModel)
        viewModel?.delegate = self
    }

    override func tearDownWithError() throws {
        viewModel = nil
    }

    func testGetCoins() throws {
        exp = expectation(description: "Done")
        viewModel?.getCoins()
        waitForExpectations(timeout: 10.0) { error in
            XCTAssertNil(error)
        }
        
        XCTAssertEqual(self.viewModel?.coins.count, 63)


    }

}
