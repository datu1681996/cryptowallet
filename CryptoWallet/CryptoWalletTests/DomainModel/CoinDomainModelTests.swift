//
//  CoinDomainModelTests.swift
//  CryptoWalletTests
//
//  Created by Tu Doan on 25/02/2022.
//

import XCTest
@testable import CryptoWallet

class CoinDomainModelTests: XCTestCase {
    var domainModel: CoinDomainModel?
    
    override func setUpWithError() throws {
        let mockData = FakeData.shared.getJSON_Data(fileName: "currencyList")
        let session = URLSessionMock()
        session.data = mockData
        let manager = Network(session: session)
        domainModel = CoinDomainModel(coinAPI: manager)
        
    }
    
    override func tearDownWithError() throws {
        domainModel = nil
    }

    func testSuccessfulResponse() throws {
        var result: [CoinDetailModel] = []
        let exp = expectation(description: "done")
        domainModel?.getCoins { data,_ in
            DispatchQueue.main.async {
                result = data
                
                XCTAssertEqual(result.count, 63)
                exp.fulfill()
            }
        }
       
        waitForExpectations(timeout: 10.0) { error in
            XCTAssertNil(error)
        }
        
    }

}
