//
//  CurrencyViewModel.swift
//  CryptoWallet
//
//  Created by Tu Doan on 22/02/2022.
//

import UIKit

final class CurrencyViewModel {
    var coin: CoinDetailModel?
    var favorImg: UIImage? {
        let isFavorite = coin?.isFavorite ?? false
        let favorImg = isFavorite ? UIImage(named: AssetName.yellowStar): UIImage(named: AssetName.grayStar)
        let desiredImg = favorImg?.resizeImage(targetSize: CGSize(width: 90, height: 90))
        return desiredImg
    }
}
