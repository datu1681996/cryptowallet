//
//  HomeViewModel.swift
//  CryptoWallet
//
//  Created by Tu Doan on 21/02/2022.
//

import CoreGraphics

protocol HomeViewModelDelegate: AnyObject {
    func onComplete(isError: Bool)
}
final class HomeViewModel {
    private let coinDomainModel: CoinDomainModelProtocol
    weak var delegate: HomeViewModelDelegate?
    var originData: [CoinDetailModel] = []
    var coins: [CoinDetailModel] = []
    let cellHeight: CGFloat = 90
    let refreshTimeInterval: Double = 30
    var numberOfRows: Int {
        return coins.count
    }
    
    init(coinDomainModel: CoinDomainModelProtocol = CoinDomainModel()) {
        self.coinDomainModel = coinDomainModel
    }
    
    func getCoins() {
        coinDomainModel.getCoins { data, isError in
            self.coins = data
            self.originData = data
            DispatchQueue.main.async {
                self.delegate?.onComplete(isError: isError)
            }
        }
    }
    
    func filterCoins(by searchText: String) {
        let text = searchText.uppercased()
        if text != "" {
            coins = originData.filter {
                $0.base.contains(text)
            }
        } else {
            coins = originData
        }
    }
}
