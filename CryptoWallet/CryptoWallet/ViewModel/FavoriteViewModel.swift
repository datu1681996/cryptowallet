//
//  FavoriteViewModel.swift
//  CryptoWallet
//
//  Created by Tu Doan on 22/02/2022.
//

import CoreGraphics

final class FavoriteViewModel {
    var originData: [CoinDetailModel] = []
    var coins: [CoinDetailModel] = []
    let cellHeight: CGFloat = 90
    var numberOfRows: Int {
        return coins.count
    }
    
    func filterFavorite() {
        coins = originData.filter { $0.isFavorite }
    }
}

