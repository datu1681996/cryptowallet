//
//  CoinTableViewCell.swift
//  CryptoWallet
//
//  Created by Tu Doan on 20/02/2022.
//

import UIKit

class CoinTableViewCell: UITableViewCell {
    @IBOutlet weak var coinImageVIew: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var buyPriceLabel: UILabel!
    @IBOutlet weak var percentChangeLabel: UILabel!
    @IBOutlet weak var percentChangeImageView: UIImageView!
    
    static var typeName: String {
        return String(describing: self)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        coinImageVIew.image = nil
        nameLabel.text = nil
        buyPriceLabel.text = nil
        buyPriceLabel.textAlignment = .right
        percentChangeLabel.text = nil
        percentChangeLabel.textAlignment = .right
        percentChangeImageView.image = nil
    }

    func display(coin: CoinDetailModel) {
        nameLabel.text = coin.base
        coinImageVIew.image = coin.iconImage
        buyPriceLabel.text = "buy: \(coin.buyPrice)"
        setPercent(percent: coin.percentChange)

    }
    
    private func setPercent(percent: Double) {
        if percent < 0 {
            percentChangeLabel.textColor = .red
            percentChangeLabel.text = String(format: "%.3f", percent)
            percentChangeImageView.image = UIImage(named: AssetName.downArrow)
        } else {
            percentChangeLabel.textColor = .systemGreen
            percentChangeLabel.text = String(format: "+ %.3f", percent)
            percentChangeImageView.image = UIImage(named: AssetName.upArrow)
        }
    }
}

private extension UILabel {
    func setPercent(percent: Double) {
        if percent < 0 {
            textColor = .red
            text =  String(format: "- %.3f", percent)
        } else {
            textColor = .systemGreen
            text =  String(format: "+ %.3f", percent)
        }
    }
}
