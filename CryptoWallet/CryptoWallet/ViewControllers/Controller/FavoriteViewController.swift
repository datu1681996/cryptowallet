//
//  FavoriteViewController.swift
//  CryptoWallet
//
//  Created by Tu Doan on 20/02/2022.
//

import UIKit

final class FavoriteViewController: UIViewController {
    @IBOutlet weak var favoriteTableView: UITableView!
    
    let favoriteVM = FavoriteViewModel()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        favoriteVM.filterFavorite()
        favoriteTableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        title = "Cryto wallet"
        favoriteTableView.dataSource = self
        favoriteTableView.delegate = self
        favoriteTableView.register(UINib(nibName: CoinTableViewCell.typeName, bundle: nil), forCellReuseIdentifier: CoinTableViewCell.typeName)
    }
    
}

extension FavoriteViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        favoriteVM.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CoinTableViewCell.typeName, for: indexPath) as! CoinTableViewCell
        cell.display(coin: favoriteVM.coins[indexPath.row])
        
        return cell
    }
    
    
}

extension FavoriteViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        favoriteVM.cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let controller = UIStoryboard(name: "Main", bundle: Bundle.main)
                            .instantiateViewController(withIdentifier: ViewName.currency) as! CurrencyViewController
        controller.currencyVM.coin = favoriteVM.coins[indexPath.row]
       navigationController?.pushViewController(controller, animated: true)
    }
}
