//
//  ViewController.swift
//  CryptoWallet
//
//  Created by Tu Doan on 20/02/2022.
//

import UIKit

extension UIViewController {
    func removeChildController() {
        navigationController?.popToViewController(self, animated: false)
    }
}

final class HomeViewController: UIViewController {
    @IBOutlet weak var coinSearchBar: UISearchBar!
    @IBOutlet weak var coinTableView: UITableView!
    
    let homeVM = HomeViewModel()
    let spinnerController = SpinnerViewController()
    private var timer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
    
        setupUI()
        homeVM.getCoins()
        homeVM.delegate = self
//        updateData()
    }

    private func setupUI() {
        title = "Cryto wallet"
        setupTableView()
        setupAndStartSpinner()
    }
    
    private func updateData() {
        self.timer = Timer.scheduledTimer(withTimeInterval: homeVM.refreshTimeInterval,
                                          repeats: true) { [weak self] _ in
            guard let self = self else { return }
            print("start refresh data")
            self.showToast(message: "refresh Data", font: .systemFont(ofSize: 15))
            self.homeVM.getCoins()
        }
    }
    
    private func setupAndStartSpinner() {
        addChild(spinnerController)
        spinnerController.view.frame = view.frame
        view.addSubview(spinnerController.view)
        spinnerController.didMove(toParent: self)
    }
    
    private func setupTableView() {
        coinSearchBar.delegate = self
        coinTableView.delegate = self
        coinTableView.dataSource = self
        coinTableView.register(UINib(nibName: CoinTableViewCell.typeName, bundle: nil), forCellReuseIdentifier: CoinTableViewCell.typeName)
    }
    
    private func showToast(message : String, font: UIFont) {

        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = .black.withAlphaComponent(0.6)
        toastLabel.textColor = .white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
             toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
}

extension HomeViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        homeVM.filterCoins(by: searchText.uppercased())
        
        coinTableView.reloadData()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        coinSearchBar.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        coinSearchBar.endEditing(true)
    }

}

extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        homeVM.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CoinTableViewCell.typeName, for: indexPath) as! CoinTableViewCell
        cell.display(coin: homeVM.coins[indexPath.row])
        
        return cell
    }
    
    
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        homeVM.cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let controller = UIStoryboard(name: "Main", bundle: Bundle.main)
                            .instantiateViewController(withIdentifier: ViewName.currency) as! CurrencyViewController
        controller.currencyVM.coin = homeVM.coins[indexPath.row]
       navigationController?.pushViewController(controller, animated: true)
    }
}

extension HomeViewController: HomeViewModelDelegate {
    func onComplete(isError: Bool) {
        if isError {
            let alertController = UIAlertController(title: "Error",
                                                    message: "Can't get data, API failed",
                                                    preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alertController, animated: true, completion: nil)
        } else {
            coinTableView.reloadData()
        }
        
        spinnerController.stopAnimating()
        updateData()
    }
}
