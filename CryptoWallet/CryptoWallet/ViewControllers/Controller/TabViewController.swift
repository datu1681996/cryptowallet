//
//  TabViewController.swift
//  CryptoWallet
//
//  Created by Tu Doan on 20/02/2022.
//

import UIKit

final class TabViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        delegate = self
    }

}

extension TabViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        viewController.children.first?.removeChildController()
        if let firstVC = viewController.children.first,
            firstVC is FavoriteViewController {
            let homeVC = tabBarController.viewControllers?[0].children.first as! HomeViewController
            let favoriteVC = firstVC as! FavoriteViewController
            // guarantee assign 1st time
            favoriteVC.favoriteVM.originData = homeVC.homeVM.coins
        }
    }
    
}
