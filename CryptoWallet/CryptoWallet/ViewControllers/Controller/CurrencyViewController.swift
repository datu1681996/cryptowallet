//
//  CurrencyViewController.swift
//  CryptoWallet
//
//  Created by Tu Doan on 20/02/2022.
//

import UIKit

final class CurrencyViewController: UIViewController {
    @IBOutlet weak var coinImageView: UIImageView!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var baseNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var transactionInLabel: UILabel!
    @IBOutlet weak var buyTitleLabel: UILabel!
    @IBOutlet weak var buyLabel: UILabel!
    @IBOutlet weak var sellTitleLabel: UILabel!
    @IBOutlet weak var sellLabel: UILabel!
    
    let currencyVM = CurrencyViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        displayCurrency()
    }

    private func setupUI() {
        buyTitleLabel.setup(title: "Buy price:")
        sellTitleLabel.setup(title: "Sell price:")
        favoriteButton.setTitle("", for: .normal)
    }
    
    private func displayCurrency() {
        guard let coin = currencyVM.coin else { return }
        title = coin.base
        coinImageView.image = coin.iconImage
        baseNameLabel.text = "Name: \(coin.base)"
        descriptionLabel.text = "Description: \(coin.name)"
        transactionInLabel.text = "Transaction in: \(coin.counter)"
        buyLabel.text = coin.buyPrice
        sellLabel.text = coin.sellPrice
        favoriteButton.setImage(currencyVM.favorImg, for: .normal)
    }

    @IBAction func setFavorite(_ sender: UIButton) {
        let favoriteStatus = currencyVM.coin?.isFavorite ?? false
        currencyVM.coin?.isFavorite = !favoriteStatus
        sender.setImage(currencyVM.favorImg, for: .normal)
    }
    
    deinit {
        print("deinit \(String(describing: self))")
    }
}

private extension UILabel {
    func setup(title: String) {
        backgroundColor = .systemGray5
        text = title
        font = .italicSystemFont(ofSize: 15)
    }
}
