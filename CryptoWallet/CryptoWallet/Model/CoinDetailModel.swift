//
//  CoinDetailModel.swift
//  CryptoWallet
//
//  Created by Tu Doan on 21/02/2022.
//

import UIKit

struct CoinDetailModel: Codable {
    var base: String
    var counter: String
    var buyPrice: String
    var icon: String
    var sellPrice: String
    var name: String
    var iconImage: UIImage?
    var isFavorite: Bool {
        get {
            guard
                let isChecked = UserDefaults.standard.object(forKey: base) as? Bool else {
                    UserDefaults.standard.set(false, forKey: base)
                    return false
                }
            
            return  isChecked
        }
        
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: base)
        }
    }
    
    var percentChange: Double {
        guard let buyIn = Double(buyPrice),
              let sellOut = Double(sellPrice) else {
                  return 0
              }
        
        let percentChange = ((buyIn - sellOut) / buyIn) * 100
        return percentChange
    }
    
    enum CodingKeys: String, CodingKey {
        case base
        case counter
        case buyPrice = "buy_price"
        case icon
        case sellPrice = "sell_price"
        case name
    }
    
}

extension CoinDetailModel {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        base = try container.decode(String.self, forKey: .base)
        counter = try container.decode(String.self, forKey: .counter)
        buyPrice = try container.decode(String.self, forKey: .buyPrice)
        icon = try container.decode(String.self, forKey: .icon)
        sellPrice = try container.decode(String.self, forKey: .sellPrice)
        name = try container.decode(String.self, forKey: .name)
        iconImage = icon.toImage()
    }
    
    func encode(to encoder: Encoder) throws {
        print("set this later")
    }
}
