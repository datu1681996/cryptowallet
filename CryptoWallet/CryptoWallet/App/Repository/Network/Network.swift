//
//  Network.swift
//  CryptoWallet
//
//  Created by Tu Doan on 20/02/2022.
//

import Foundation

typealias Response = (([CoinDetailModel], HTTPURLResponse?, DataError?, Error?) -> Void)
typealias CoinNetworkData = [String: [CoinDetailModel]]

protocol CoinAPI {
    func query(completionHandler: @escaping Response)
}

final class Network: CoinAPI {
    private var interval = 10.0
    private var session: URLSession
    
    init(session: URLSession = .shared) {
        self.session = session
    }
        
    func query(completionHandler: @escaping Response) {
        DispatchQueue.global(qos: .default).async {
            
            if let url = URL(string: "https://www.coinhako.com/api/v3/price/all_prices_for_mobile?counter_currency=USD") {
                
                let urlRequest = URLRequest(url: url,
                                            cachePolicy: .reloadIgnoringLocalCacheData,
                                            timeoutInterval: self.interval)

                let dataTask = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
                    
                        let httpResponse = response as? HTTPURLResponse
                        guard let data = data,
                              let coinNetworkData = try? JSONDecoder().decode(CoinNetworkData.self, from: data),
                              let coinData = coinNetworkData["data"] else {
                                  completionHandler([], nil, .invalidModel, nil)
                                  return
                        }
                        
                        if let error = error {
                            completionHandler([], httpResponse, nil, error)
                            return
                        }
                        
                        completionHandler(coinData, httpResponse, nil, nil)
                }
                
                dataTask.resume()
            } else {
                DispatchQueue.main.async {
                    completionHandler([], nil, .invalidURL, nil)
                }
            }
        }
    }
    
}
