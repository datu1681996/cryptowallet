//
//  Constant.swift
//  CryptoWallet
//
//  Created by Tu Doan on 21/02/2022.
//

import Foundation

enum AssetName {
    static let upArrow = "up_arrow"
    static let downArrow = "down_arrow"
    static let grayStar = "gray_star"
    static let yellowStar = "yellow_star"
}

enum ViewName {
    static let currency = "CurrencyViewController"
}
