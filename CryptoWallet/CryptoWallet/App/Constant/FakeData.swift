//
//  FakeData.swift
//  CryptoWallet
//
//  Created by Tu Doan on 25/02/2022.
//

import Foundation

final class FakeData {
    static let shared = FakeData()
    private init () { }

    func getJSON_Data(fileName: String) -> Data? {
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            do {
                  let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                  return data
              } catch {
                    print("error, not convertible")
              }
        }
        return nil
    }
    
    func getSampleCoins() -> [CoinDetailModel] {
        let data = getJSON_Data(fileName: "currencyList")
        do {
            let result = try JSONDecoder().decode([CoinDetailModel].self, from: data ?? Data())
            return result
        } catch {
            return []
        }
    }
}
