//
//  Error.swift
//  CryptoWallet
//
//  Created by Tu Doan on 21/02/2022.
//

import Foundation

enum DataError: Error {
    case invalidURL
    case invalidModel
}
