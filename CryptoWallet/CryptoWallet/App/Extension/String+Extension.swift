//
//  String+Extension.swift
//  CryptoWallet
//
//  Created by Tu Doan on 21/02/2022.
//

import UIKit

extension String {
    func toImage() -> UIImage? {
        guard let url = URL(string: self),
              let data = try? Data(contentsOf: url),
              let image = UIImage(data: data) else {
                  return nil
              }

        return image
    }
    
}
