//
//  CoinDomainModel.swift
//  CryptoWallet
//
//  Created by Tu Doan on 21/02/2022.
//

import Foundation

protocol CoinDomainModelProtocol {
    func getCoins(completionHandler: @escaping ([CoinDetailModel], Bool) -> Void)
}

final class CoinDomainModel: CoinDomainModelProtocol {
    private let coinAPI: CoinAPI
    init(coinAPI: CoinAPI = Network()) {
        self.coinAPI = coinAPI
    }
    
    func getCoins(completionHandler: @escaping ([CoinDetailModel], Bool) -> Void) {
        coinAPI.query { data, response, dataError, error in
            if let error = error {
                print("network error: \(error)")
                print("code: \(String(describing: response?.statusCode))")
                completionHandler([], true)
            } else if let dataError = dataError {
                print("data error: \(dataError)")
                completionHandler([], true)
            }else {
                completionHandler(data, false)
            }
                
        }
    }
    
    
}
