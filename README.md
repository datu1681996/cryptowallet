# CryptoWallet
A digital currencies wallet for CH Resourse Vietnam

# Note
For simplicity, I don't use third party frameworks. So just run the .xcodeproj file directly, no extra installation.
When run real device, change Team and bundle id in Signing setting if necessary.
I use MVVM architecture in this project.
Since only 1 API call, I don't use persistent store.
